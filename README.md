# Plotter
Enter an analytic function and plot it between two points of its domain.

# Plotter2
Define into FUNCTION.h an analytic 2-variable function and plot it in 3D with gnuplot.

# Usage
To run Plotter.c simply execute bash script with:
    
   
    bash start.sh


For Plotter2 define into FUNCTION.h an f(x,y) (parametric functions or explicit cartesian equation) and execute:
    
    
    bash start.sh


changing the gcc command in 'start.sh' script depending on the type of function;
if it is not parametric comment the first gcc command, otherwise the second. 

# Contrib
Written with the tinyexpr library: https://github.com/codeplea/tinyexpr
