// Coded by ScratchyCode
    
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "FUNCTION.h"

void checkPtr(void *ptr);

int main(void){
    long double xinf, xsup, yinf, ysup;
    long double xinc, yinc;
    long double x, y;
    
    printf("Enter the lower x: ");
    scanf("%Lf",&xinf);
    
    printf("Enter the upper x: ");
    scanf("%Lf",&xsup);
    
    printf("Enter the lower y: ");
    scanf("%Lf",&yinf);
    
    printf("Enter the upper y: ");
    scanf("%Lf",&ysup);
    
    printf("Enter the increment for x: ");
    scanf("%Lf",&xinc);
    
    printf("Enter the increment for y: ");
    scanf("%Lf",&yinc);
    
    // check parameters
    if(xinc <= 0 || yinc <= 0 || xinf >= xsup || yinf >= ysup){
        fprintf(stderr,"\n*** invalid parameters ***\n");
        exit(1);
    }
    
    FILE *output = fopen("graph.dat","w");
    checkPtr(output);
    
    // estimate elaboration percentage
    unsigned long long int progress = 0;
    double runs = (double)(fabsl(xsup - xinf)/xinc);
    
    printf("\n");
    
    x = xinf;
    do{
        progress++;
        fprintf(stderr,"\rcalculating:\t\t%2d%%",(int)((progress * 100.)/runs));
        
        y = yinf;
        do{
            #ifdef PARAMETRIC
            fprintf(output,"%Lf\t%Lf\t%Lf\n",i(x,y),j(x,y),k(x,y));
            #else
            fprintf(output,"%Lf\t%Lf\t%Lf\n",x,y,f(x,y));
            #endif
            y += yinc;
        }while(y <= ysup);
        
        x += xinc;
    }while(x <= xsup);
    
    //fprintf(stderr,"\n\n*** done ***\n");
    fprintf(stderr,"\n");
    fflush(output);
    fclose(output);
    
    return 0;
}

void checkPtr(void *ptr){
    
    if(ptr == NULL){
        perror("\nERROR");
        fprintf(stderr,"\n");
        exit(0);
    }
    
    return;
}

