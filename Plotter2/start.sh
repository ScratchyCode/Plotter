#!/bin/bash
# Coded by Scratchy

#gcc plotter2.c -lm -O3 -o plotter2.exe -D PARAMETRIC # if f(x,y) is defined parametrically
gcc plotter2.c -lm -O3 -o plotter2.exe # if f(x,y) is in explicit form

./plotter2.exe

echo "Plotting..."
gnuplot --persist script.gp

echo "Done."

exit
