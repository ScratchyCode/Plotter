set autoscale x
set autoscale y
set autoscale z
set xlabel 'x'
set ylabel 'y'
set zlabel 'z'
set title 'f(x,y) 3D plot'

splot 'graph.dat' u 1:2:3 w d
pause mouse close

#set terminal png size 1920,1080
set terminal png
set output 'plot.png'
splot 'graph.dat' u 1:2:3 w d
