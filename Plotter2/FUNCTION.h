/*** EXPLICIT FUNCTIONS ***/

//#define f(x,y) sqrt(1 - ((x*x) + (y*y))) // semisfera positiva
//#define f(x,y) -sqrt(1 - ((x*x) + (y*y))) // semisfera negativa
#define f(x,y) (sin((x*x) + (y*y))/((x*x) + (y*y))) // onde
//#define f(x,y) ((x*x) - (y*y)) // pringles
//#define f(x,y) ((x*x*x) - (4*x*y*y))
//#define f(x,y) ((x*y)/((x*x) + (y*y))) // singolare in (0,0)
//#define f(x,y) (((x-2)*(x-2)*(y-1))/(((x-2)*(x-2)*(x-2)*(x-2)) + ((y-1)*(y-1))))



/*** PARAMETRIC FUNCTIONS ***/

// Nastro di Moebius (x tra (-5,5) e y tra (0,6.28))
#define i(x,y) ((10 + x * sin(y/2)) * cos(y))
#define j(x,y) ((10 + x * sin(y/2)) * sin(y))
#define k(x,y) (x * cos(y/2))

